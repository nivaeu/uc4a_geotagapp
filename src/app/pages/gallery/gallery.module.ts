import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { GalleryPage } from './gallery.page';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { httpLoaderFactory } from '../../app.module';
import { HttpClient } from '@angular/common/http';
import { LongPressModule } from 'ionic-long-press';
import { PinchZoomModule } from 'ngx-pinch-zoom';

@NgModule({

  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    LongPressModule,
    PinchZoomModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: httpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    RouterModule.forChild([{ path: '', component: GalleryPage }])
  ],
  declarations: [GalleryPage]
})
export class GalleryPageModule { }
