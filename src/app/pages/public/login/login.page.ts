import { Component, OnInit, ElementRef } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { AuthenticationService } from '../../../services/authentication.service';
import { StorageService } from '../../../services/storage.service';
import { isPlatform } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  accessToken = {
    digit1: '',
    digit2: '',
    digit3: '',
    digit4: '',
    digit5: ''
  }
  errorString: string = null;

  acceptTerms = {
    value: "accepted",
    isChecked: false,
  }

  TermsValue: boolean = false;

  constructor(
    private auth: AuthenticationService,
    private storageService: StorageService,
    private requestAPI: ApiService
  ) { }

  ngOnInit() {
    this.storageService.getLocalData('accepted').then(res => {
      if (res !== null) {
        this.acceptTerms = JSON.parse(res);
        this.TermsValue = this.acceptTerms.isChecked;
        console.log(this.acceptTerms);
      }
    })
  }
  // NOT working on Android : revisit on next release
  moveToNext(event, number) {

    if (isPlatform('android')) {
      return;
    }

    if (number === 6) {
      return;
    }
    if (event.explicitOriginalTarget.value.length === 1) {

      let nextControl: any = document.getElementsByName('digit' + number);

      console.log('test', nextControl);
      nextControl = nextControl[0];

      // Searching for next similar control to set it focus
      if (nextControl) {
        console.log(nextControl.firstChild);
        nextControl.firstChild.focus();
        return;
      }
      else {
        nextControl = nextControl.nextElementSibling;
      }
    }
    else {
      return;
    }
  }


  /**
   * on checkbox change
   */
  checkTerms(event): void {
    this.TermsValue = event.detail.checked;
  }
  async SubmitToken() {
    const token = this.accessToken.digit1 + this.accessToken.digit2 +
      this.accessToken.digit3 + this.accessToken.digit4 + this.accessToken.digit5;
    const upperToken = token.toUpperCase();

    /* this.requestAPI.getRequests(upperToken).then(response => {

      if (response.status === 200) {
        // needs to be reinstated for 5 digit code auth
        // this.auth.login(true);
        console.log(response.data);
        this.storageService.setLocalData('requests', response.data);
        this.storageService.setLocalData('login-token', upperToken);
        this.acceptTerms.isChecked = this.TermsValue;
        this.storageService.setLocalData('requests', this.requestAPI.requests);
        this.storageService.setLocalData('login-token', this.requestAPI.token);
        this.storageService.setLocalData('accepted', JSON.stringify(this.acceptTerms));
      
      }
      else {
        this.errorString = 'That code is invalid, please try again'
      }
    })
      .catch(error => {
        this.errorString = 'That was an error, please try again (' + error.status + ')';
      }); */
  }
}


