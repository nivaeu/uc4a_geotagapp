import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { acceptedTerms } from 'config';
import { AudioService } from 'src/app/services/audio.service';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { StorageService } from 'src/app/services/storage.service';
@Component({
  selector: 'app-sso-login',
  templateUrl: './sso-login.page.html',
  styleUrls: ['./sso-login.page.scss'],
})
export class SsoLoginPage implements OnInit {
  TermsValue = false;
  playing = false;
  constructor(private auth: AuthenticationService,
              private storageService: StorageService,
              private audio: AudioService,
              private cd: ChangeDetectorRef) {}

  ngOnInit(): void {
    this.playing = false;
    this.audio.getLoginAudioFile();
    this.audio.loginAudioFile.onStatusUpdate.subscribe(status => {
      if (status === 4){
        this.playing = false;
        this.cd.detectChanges();
        this.audio.loginAudioFile.release();
      }
    });
    this.storageService.getLocalData(acceptedTerms).then(res => {
      if (res !== null) {
        this.auth.acceptTerms = JSON.parse(res);
        this.TermsValue = this.auth.acceptTerms.isChecked;
        console.log(JSON.stringify(this.auth.acceptTerms));
      }
    });
  }

  async login() {
    this.auth.authenticate(this.TermsValue);
  }

  playAudio(){
    if(!this.playing) {
      this.playing = true;
      this.audio.loginAudioFile.setVolume(1);
      this.audio.loginAudioFile.play();
    } else {
      this.playing = false;
      this.audio.loginAudioFile.pause();
    }
  }

  checkTerms(event): void {
    this.TermsValue = event.detail.checked;
  }

  ionViewDidLeave(){
    this.audio.loginAudioFile.release();
  }
}
