import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SsoLoginPage } from './sso-login.page';

const routes: Routes = [
  {
    path: '',
    component: SsoLoginPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SsoLoginPageRoutingModule {}
