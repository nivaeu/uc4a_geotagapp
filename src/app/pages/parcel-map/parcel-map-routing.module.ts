import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ParcelMapPage } from './parcel-map.page';

const routes: Routes = [
  {
    path: '',
    component: ParcelMapPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ParcelMapPageRoutingModule {}
