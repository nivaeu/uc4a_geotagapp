import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { IonicModule } from '@ionic/angular';
import { httpLoaderFactory } from '../../app.module';
import { HttpClient } from '@angular/common/http';
import { PhotoDetailsPageRoutingModule } from './photo-details-routing.module';
import { PhotoDetailsPage } from './photo-details.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PhotoDetailsPageRoutingModule,
    TranslateModule.forChild({
      loader: {
        provide: TranslateLoader,
        useFactory: httpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [PhotoDetailsPage]
})
export class PhotoDetailsPageModule { }
