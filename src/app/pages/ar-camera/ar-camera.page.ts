import { Component, OnInit, ViewChild, ElementRef, HostListener, } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { ArCameraService } from 'src/app/services/ar-camera.service';
import { ActivatedRoute } from '@angular/router';
import { PhotoService } from 'src/app/services/photo.service';
import { DomSanitizer } from '@angular/platform-browser';
import { id } from '../../../../config';
import { LocationService } from 'src/app/services/location.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-ar-camera',
  templateUrl: './ar-camera.page.html',
  styleUrls: ['./ar-camera.page.scss'],
})
export class ArCameraPage implements OnInit {

  constructor(
    private locationAPI: ApiService,
    private geolocation: Geolocation,
    public arCameraService: ArCameraService,
    private route: ActivatedRoute,
    private photoService: PhotoService,
    private sanitizer: DomSanitizer,
    public location: LocationService,
    private translate: TranslateService
  ) { }

  @ViewChild('ArFrame', { static: false })
  ArFrame: ElementRef;

  refLatitude: string;
  refLongitude: string;
  myLat: string;
  myLng: string;
  ARUrl: string;
  safeUrl: any;
  hashId: string = null;
  reap = false;
  markerStatus: string;
  contains: string;

  ionViewWillEnter() {
    this.photoService.loadSaved();
    // get id from routing param
    this.hashId = this.route.snapshot.paramMap.get(id);
    if (this.hashId !== null || this.hashId === '') {

      if (this.hashId === 'REAP' || this.hashId === 'FES' || this.hashId === 'AECM') {

        this.reap = true;
      }

      this.arCameraService.cameraActive = true;
      this.arCameraService.openCamera();
    }

    this.translate.get('LandParcel.Contains').subscribe(value => {
      this.contains = value;
    });
  }

  async ngOnInit() {
    // construct URL for AFrame
    this.ARUrl = `../../../assets/aframe-ar.html`;
  }

  hideFrame() {
    if (this.ArFrame.nativeElement.style.display === 'none') {
      this.ArFrame.nativeElement.style.display = 'block';
      this.translate.get('Map.HideMarker').subscribe(value => {
        this.markerStatus = value;
      });
    } else {
      this.ArFrame.nativeElement.style.display = 'none';
      this.translate.get('Map.ShowMarker').subscribe(value => {
        this.markerStatus = value;
      });
    }
  }


  ionViewDidEnter() {
    if (!this.reap) {
      this.translate.get('Map.HideMarker').subscribe(value => {
        this.markerStatus = value;
      });
      const refLatitude = this.locationAPI.referencePoint.referencePoint.coordinates[1];
      const refLongitude = this.locationAPI.referencePoint.referencePoint.coordinates[0];
      try {
        const ArElement = this.ArFrame.nativeElement;
        const ArWindow = ArElement.contentWindow;
        this.location._Location.subscribe({
          next(pos) {
            const data: Message = {
              myLat: pos.coords.latitude,
              myLng: pos.coords.longitude,
              altitude: pos.coords.altitude,
              heading: pos.coords.heading,
              refLat: refLatitude,
              refLng: refLongitude,
            }

            const stringify = JSON.stringify(data);
            ArWindow.postMessage(stringify, '*')
          }
        });
      } catch (e) {
        console.log('~', e.message);
      }
    } else {
      return;
    }
  }

  // listening event for changes to AR environment
  /* @HostListener('window:message', ['$event']) viewMessage(event){
    try {
      console.log('23scanlop ', JSON.stringify(event.data));
    } catch(e) {
      console.log('2scanlop ', e.message);
    }
  } */

  addtoGallery() {
    console.log('scanlop ', this.hashId)
    this.location.photoDetails.requestId = this.hashId;
    this.arCameraService.captureImage(this.hashId);
  }

  ionViewDidLeave(): void {
    const snapshot = this.route.snapshot;
    const params = { ...snapshot.queryParams };
    delete params.pos;
    if (this.arCameraService.cameraActive) {
      this.arCameraService.stopCamera();
      this.arCameraService.cameraActive = false;
    }
  }
}

interface Message {
  myLat: any;
  myLng: any;
  altitude: any;
  heading: any;
  refLat: number;
  refLng: number;
}
