import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api.service';
import * as Leaflet from 'leaflet';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { AlertController, isPlatform, LoadingController, ToastController } from '@ionic/angular';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { urlTemplate } from '../../../../config'
import { AudioService } from 'src/app/services/audio.service';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { PermissionType, Plugins } from '@capacitor/core';
import { LocationService } from 'src/app/services/location.service'
const { Permissions } = Plugins
@Component({
  selector: 'app-land-parcels',
  templateUrl: 'land-parcels.page.html',
  styleUrls: ['land-parcels.page.scss'],
})
export class LandParcelsPage implements OnInit {
  constructor(
    private translate: TranslateService,
    private locationAPI: ApiService,
    private auth: AuthenticationService,
    private geolocation: Geolocation,
    private loadingController: LoadingController,
    private toastController: ToastController,
    private go: Router,
    private audio: AudioService,
    private cd: ChangeDetectorRef,
    private androidPermissions: AndroidPermissions,
    private locationAccuracy: LocationAccuracy,
    public alertController: AlertController,
    private diagnostic: Diagnostic,
    private locationService: LocationService
  ) { }

  playing = false;
  refLat: number;
  refLong: number;
  landParcelMap: Leaflet.Map;
  marker: any;
  firstLoad = false;
  private toast: string;
  deviceMarker = Leaflet.icon({
    iconUrl: './assets/images/location.svg',
    iconAnchor: [25, 25]
  })
  landParcel = [] as IParcel[];
  selectedParcel: any;
  selectedScheme: any;
  permission = false;
  alertHeader: string;
  alertSubHeader: string;
  alertButton: string;
  ngOnInit() {

  }

  playAudio() {
    if (!this.playing) {
      this.playing = true;
      this.audio.landParcelsAudioFile.setVolume(1);
      this.audio.landParcelsAudioFile.play();
    } else {
      this.playing = false;
      this.audio.landParcelsAudioFile.pause();
    }
  }

  async ionViewWillEnter() {
    this.getTranslations();
    this.permission = false;
    this.selectedParcel = undefined;
    this.selectedScheme = undefined;
    this.playing = false;
    this.audio.getLandParcelsAudioFile()
    this.audio.landParcelsAudioFile.onStatusUpdate.subscribe(status => {
      if (status === 4) {
        this.playing = false;
        this.cd.detectChanges();
        this.audio.landParcelsAudioFile.release();
      }
    });

    this.diagnostic.isLocationAvailable().then(async data => {
      if (data) {
        this.permission = true;
        this.firstLoad = true;
        this.presentLoading();
        await this.getInitialData();
      } else {
        if (isPlatform('android')) {
          this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(async location => {
            if (location.code === 0 || location.code === 1) {
              this.diagnostic.isLocationAvailable().then(async response => {
                if (response) {
                  this.permission = true;
                  this.firstLoad = true;
                  this.presentLoading();
                  await this.getInitialData();
                }
              });
            }
          }).catch(async error => {
            const alert = await this.alertController.create({
              header: this.alertHeader,
              subHeader: this.alertSubHeader,
              buttons: [this.alertButton]
            });
            alert.present();
          });
        }
      }
    });
  }

  async getInitialData() {
    this.landParcel = [];
    const posOptions = {
      enableHighAccuracy: true,
      timeout: 10000,
      maximumAge: 30000,
    };
    await this.geolocation
      .getCurrentPosition(posOptions)
      .then(async (resp) => {
        this.refLat = resp.coords.latitude;
        this.refLong = resp.coords.longitude;
        this.postLandLocations(this.refLat, this.refLong);
      });
  }

  async getPolygons(latitude: number, longitude: number) {
    for (const poly of this.landParcel) {
      this.landParcelMap.removeLayer(poly.polygon);
      this.landParcelMap.removeLayer(poly.marker);
    }
    this.landParcel = [];
    this.presentLoading();
    this.postLandLocations(latitude, longitude);
  }

  async postLandLocations(latitude: number, longitude: number) {
    console.log('scanlop lat ', latitude + ' ' + longitude)
    await this.locationAPI.postLandLocations(this.auth.currentToken, latitude, longitude).then(response => {
      if (response.status === 200) {
        const parsed = JSON.parse(response.data);
        const array: any[] = [];
        if (typeof parsed[0] !== 'undefined') {
          for (const value of parsed) {
            array.push(value);
          }
          for (const value of array) {
            const parcel = {} as IParcel;
            parcel.label = value.label;
            const coordsArray: any[] = [];
            for (const coords of value.geoCoordinates.coordinates[0]) {
              const lat = coords[1];
              const lng = coords[0];
              coordsArray.push({ lng, lat });
            }
            parcel.polygon = new Leaflet.Polygon(coordsArray);
            // get centre of polygon
            const centre = parcel.polygon.getBounds().getCenter();
            parcel.referencePoint = centre;
            // calculate distance
            const distance = this.locationService.HaversineFormula(latitude, centre.lat, longitude, centre.lng);
            parcel.distance = distance;
            // add a marker
            parcel.marker = new Leaflet.marker([centre.lat, centre.lng]);
            this.landParcel.push(parcel);
          }
          this.landParcel.sort((a, b) => (a.distance < b.distance ? -1 : 1 ));
          if (!this.firstLoad) {
            for (const coords of this.landParcel) {
              // const centre = coords.polygon.getBounds().getCenter();
              const contentString =
                '<div id="content" style="color: #666;"><strong> Land Parcel: ' +
                coords.label +
                '</strong>';
              // routerLink="\'/terms-and-conditions/\'"
              // <br/><button id="myButton" type="button" class="btn btn-primary" (click)="takeImage()" >Click me!</button>';
              // add a marker
              /* let name : any = 'link_' + coords.label;
              let event : any = 'eventHandlerAssigned_' + coords.label;
              event = false;
              this.landParcelMap.on('popupopen', (e) => {
                if (!event && document.querySelector('.link')) {
                  console.log('Inside if')
                  name = document.querySelector('.link')
                  name.addEventListener('click', () => {
                    this.test(coords.label);
                  });
                  event = true
                }
              });
             this.landParcelMap.on('popupclose', (e) => {
                name.removeEventListener('click', this.test);
                event = false
              }); */
              coords.marker.addTo(this.landParcelMap).bindPopup(contentString);
              coords.polygon.addTo(this.landParcelMap);
              this.loadingController.dismiss();
            }
          }
        } else {
          this.loadingController.dismiss();
        }
      } else {
        const t = this.toastController.create({
          message: this.toast,
          position: 'top',
          duration: 3000,
          cssClass: 'custom-toast-warning',
          buttons: [
            {
              side: 'start',
              icon: 'alert-circle-outline',
              handler: () => {
                console.log('');
              },
            },
          ],
        });
        // tslint:disable-next-line: no-shadowed-variable
        t.then((t) => t.present());
        this.loadingController.dismiss();
      }
    }).catch(error => {
      if (error.status === 401) {
        let text: string;
        this.translate.get('Toast.Expired').subscribe(value => {
          text = value;
        });
        const t = this.toastController.create({
          message: text,
          position: 'top',
          duration: 3000,
          cssClass: 'custom-toast-warning',
          buttons: [
            {
              side: 'start',
              icon: 'alert-circle-outline',
              handler: () => {
                console.log('');
              },
            },
          ],
        });
        // tslint:disable-next-line: no-shadowed-variable
        t.then((t) => t.present());
        this.loadingController.dismiss();
        this.go.navigate(['login'])
      } else {
        console.log('DAFM error land parcels response ', JSON.stringify(error));
        const t = this.toastController.create({
          message: this.toast,
          position: 'top',
          duration: 3000,
          cssClass: 'custom-toast-warning',
          buttons: [
            {
              side: 'start',
              icon: 'alert-circle-outline',
              handler: () => {
                console.log('');
              },
            },
          ],
        });
        // tslint:disable-next-line: no-shadowed-variable
        t.then((t) => t.present());
        this.loadingController.dismiss();
      }
    });
    if (this.firstLoad) {
      this.loadMap();
      this.firstLoad = false;
    }
  }

  navigate() {
    for (const item of this.landParcel) {
      if (item.label === this.selectedParcel) {
        this.locationAPI.selectedParcel = item;
        if(this.selectedScheme === 'FSS') {
          this.locationAPI.selectedParcel.scheme = this.selectedScheme;
          this.locationAPI.selectedParcel.type = 'FODDER_EVIDENCE'
        }
        this.locationAPI.randomGenerator();
      }
    }
    this.go.navigate(['/parcel-map/' + this.locationAPI.random]);
  }

  async loadMap() {
    const t = this;
    // load map
    this.landParcelMap = Leaflet.map('landParcelMap').setView([this.refLat, this.refLong], 16);
    Leaflet.tileLayer(urlTemplate, {
      minZoom: 14,
      attribution: 'i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community <a href="http://www.esri.com/">Esri</a>',
    }).addTo(this.landParcelMap);

    // set defaults for icons , fix image bug
    // https://gis.stackexchange.com/a/357112
    const iconShadow = './assets/images/marker-shadow.png';
    const defaultIcon = {
      location: Leaflet.icon({
        iconUrl: './assets/images/marker-icon-red.png',
        iconSize: [25, 41],
        iconRetinaUrl: './assets/images/marker-icon-2x-red.png',
        shadowUrl: iconShadow,
        iconAnchor: [12, 41],
        popupAnchor: [-1, -36],
      }),
      landParcelIcon: Leaflet.icon({
        iconUrl: './assets/images/marker-icon-blue.png',
        iconSize: [25, 41],
        iconRetinaUrl: './assets/images/marker-icon-2x-blue.png',
        shadowUrl: iconShadow,
        iconAnchor: [12, 41],
        popupAnchor: [-1, -36],
      }),
    }
    Leaflet.Marker.prototype.options.icon = defaultIcon.landParcelIcon;

    this.landParcelMap.panTo(new Leaflet.LatLng(this.refLat, this.refLong));

    this.marker = Leaflet.marker([this.refLat, this.refLong], { icon: defaultIcon.location }).addTo(this.landParcelMap).bindPopup('Your location');

    this.landParcelMap.on('click', (e) => {
      if (this.marker) {
        this.landParcelMap.removeLayer(this.marker);
      }
      this.marker = new Leaflet.marker([e.latlng.lat, e.latlng.lng], { icon: defaultIcon.location }).addTo(
        this.landParcelMap
      );
      this.getPolygons(e.latlng.lat, e.latlng.lng);
    });

    for (const coords of this.landParcel) {
      // const centre = coords.polygon.getBounds().getCenter();
      const contentString = '<div id="content" style="color: #666;"><strong> Land Parcel: ' +
        coords.label +
        '</strong></div>';
      // add a marker
      coords.marker.addTo(this.landParcelMap).bindPopup(contentString);
      coords.polygon.addTo(this.landParcelMap);
    }
    this.loadingController.dismiss();
  }

  ionViewDidLeave(): void {
    this.audio.landParcelsAudioFile.release();
    this.landParcelMap.off();
    this.landParcelMap.remove();
    this.landParcelMap.stop();
  }

  async presentLoading() {
    let text: string;
    this.translate.get('Wait.Longer').subscribe(value => {
      text = value;
    });
    const loading = await this.loadingController.create({
      message: text,
      translucent: true,
    });
    return await loading.present();
  }

  doRefresh(refresher) {
    this.landParcelMap.off();
    this.landParcelMap.remove();
    this.presentLoading();
    console.log('Begin async operation', refresher);
    this.getInitialData();
    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.target.complete();
    }, 2000);
  }

  private getTranslations() {
    // tslint:disable-next-line: deprecation
    this.translate.get('Parcels.Toast').subscribe(
      value => {
        this.toast = value;
      }
    )
    this.translate.get('Map.AlertHeader').subscribe(
      value => {
        this.alertHeader = value;
      }
    )
    this.translate.get('Map.AlertSubHeader').subscribe(
      value => {
        this.alertSubHeader = value;
      }
    )
    this.translate.get('Map.AlertButton').subscribe(
      value => {
        this.alertButton = value;
      }
    )
  }
}

interface IParcel {
  polygon: any;
  label: string;
  marker: any;
  referencePoint: any;
  distance: number;
  scheme: string;
  type: string;
}
