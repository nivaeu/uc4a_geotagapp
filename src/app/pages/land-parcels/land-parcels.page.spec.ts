import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LandParcelsPage } from './land-parcels.page';

describe('LandParcelsPage', () => {
  let component: LandParcelsPage;
  let fixture: ComponentFixture<LandParcelsPage>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [LandParcelsPage],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LandParcelsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
