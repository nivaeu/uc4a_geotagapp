import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { isPlatform, Platform, ToastController } from '@ionic/angular';
import { NetworkService } from '../../services/network.service';
import { TranslateService } from '@ngx-translate/core';
import { LocationService } from '../../services/location.service';
import { StorageService } from '../../services/storage.service';
import { ApiService } from 'src/app/services/api.service';
import { LoadingController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { AudioService } from 'src/app/services/audio.service';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';

@Component({
  selector: 'app-requests',
  templateUrl: 'requests.page.html',
  styleUrls: ['requests.page.scss'],
})
export class RequestsPage implements OnInit {
  private platform: Platform;
  myTitle = 'To AgriSnap';
  id: string;
  private expired: string;
  private requestFailed: string;
  private coordinatedFailed: string;
  // notifications = [] as any[];
  constructor(
    private toastController: ToastController,
    private networkService: NetworkService,
    private storageService: StorageService,
    private requestAPI: ApiService,
    platform: Platform,
    private route: Router,
    private androidPermissions: AndroidPermissions,
    private translate: TranslateService,
    public locationService: LocationService,
    private loadingController: LoadingController,
    private auth: AuthenticationService,
    private audio: AudioService,
    private cd: ChangeDetectorRef,
    private diagnostic: Diagnostic
  ) {
    this.platform = platform;
  }
  /**
   *  on init
   * load android permissions
   */
  async ngOnInit() {
    await this.diagnostic.requestLocationAuthorization();
    if (isPlatform('android')) {
      await this.androidPermissions
        .checkPermission(this.androidPermissions.PERMISSION.READ_PHONE_STATE)
        .then(async (data: any) => {
          if (!data.hasPermission) {
            await this.androidPermissions.requestPermission(
              this.androidPermissions.PERMISSION.READ_PHONE_STATE
            );
          }
        });
    }
  }

  async ionViewWillEnter() {
    this.audio.requestPlaying = false;
    this.audio.getRequestAudioFile()
    this.audio.requestsAudioFile.onStatusUpdate.subscribe(status => {
      if (status === 4) {
        this.audio.requestPlaying = false
        this.cd.detectChanges();
        this.audio.requestsAudioFile.release();
      }
    });
    this.getTranslations();
    this.requestAPI.contexts = [];
    this.presentLoading();
    this.readData();
  }

  ionViewDidLeave() {
    this.audio.requestsAudioFile.release();
  }

  async presentLoading() {
    let text: string;
    this.translate.get('Wait.Wait').subscribe(value => {
      text = value;
    });
    const loading = await this.loadingController.create({
      message: text,
      translucent: true,
      duration: 5000
    });
    return await loading.present();
  }

  playAudio() {
    if (!this.audio.requestPlaying) {
      this.audio.requestPlaying = true;
      this.audio.requestsAudioFile.setVolume(1);
      this.audio.requestsAudioFile.play();
    } else {
      this.audio.requestPlaying = false;
      this.audio.requestsAudioFile.pause();
    }
  }

  doRefresh(refresher) {
    this.requestAPI.contexts = [];
    this.presentLoading();
    this.readData();
    setTimeout(() => {
      refresher.target.complete();
    }, 2000);
  }

  async readData() {
    // check if offline , bug in network setting offline = online
    if (
      this.networkService.getCurrentNetworkStatus() === false
    ) {
      let toast: Promise<HTMLIonToastElement>;
      // tslint:disable-next-line: deprecation
      this.translate.get('Notifications.Offline').subscribe((value) => {
        // value is our translated string
        toast = this.toastController.create({
          message: value,
          duration: 3000,
          position: 'top',
          cssClass: 'custom-toast',
          buttons: [
            {
              side: 'start',
              icon: 'cloud-offline-sharp',
              handler: () => {
                console.log('');
              },
            },
          ],
        });
      });
      // tslint:disable-next-line: no-shadowed-variable
      toast.then((toast) => toast.present());
      // load from local storage for online and offline
      // already set on login screen , checks are here
      /* this.storageService.getLocalData('requests').then((res) => {
        this.requestAPI.requests = res;
        this.storageService.getLocalData(this.requestAPI.requests.hash).then((response) => {
          this.requestAPI.requests.context = response
          this.notifications = this.requestAPI.requests.context
          this.loadingController.dismiss();
        });
      }); */

      await this.storageService.getLocalData('requests').then(async (res) => {
        this.requestAPI.requests = res;
        for (const request of this.requestAPI.requests) {
          await this.storageService.getLocalData(request.hash).then((response) => {
            for (const resp of response) {
              this.requestAPI.contexts.push(resp);
            }
          });
        }
        this.loadingController.dismiss();
      });
      this.loadingController.dismiss();
    } else {
      const requestArray = [];
      await this.requestAPI
        .getRequests(this.auth.currentToken)
        .then(async response => {
          if (response.status === 200) {
            if (this.auth.userType === 'AGENT') {
                  this.storageService.setLocalData('requests', JSON.parse(response.data));
                  this.requestAPI.requests = JSON.parse(response.data);
                  for (const request of this.requestAPI.requests) {
                    if (request.owner !== 'REAP' && request.owner !== 'FSS' && request.owner !== 'AECM' && request.owner !== 'FES') {
                      for (const context of request.context) {
                        this.storageService.setLocalData(request.hash, request.context);
                        this.requestAPI.contexts.push(context);
                        await this.getReferencePoint(context.label, context.hash);
                      }
                    }
                  }
            }
            // this else statement relates to agents - and how requests are handled for them differently as per previous requests created. 
            else {
              const parsed = JSON.parse(response.data);
              if (typeof parsed.content[0] !== 'undefined') {
                // this.storageService.setLocalData('requests', parsed.content)
                this.requestAPI.requests = parsed.content;
                for (const request of this.requestAPI.requests) {
                  if (request.owner !== 'REAP' && request.owner !== 'FSS' && request.owner !== 'AECM' && request.owner !== 'FES') {
                    requestArray.push(request);
                    await this.getContexts(request.hash, request.owner, request.type);
                  }
                }
              } else {
                this.loadingController.dismiss();
              }
              this.storageService.setLocalData('requests', requestArray);
            }

          }
        }).catch(error => {
          if (error.status === 401) {
            const t = this.toastController.create({
              message: this.expired,
              position: 'top',
              duration: 3000,
              cssClass: 'custom-toast-warning',
              buttons: [
                {
                  side: 'start',
                  icon: 'alert-circle-outline',
                  handler: () => {
                    console.log('');
                  },
                },
              ],
            });
            // tslint:disable-next-line: no-shadowed-variable
            t.then((t) => t.present());
            this.loadingController.dismiss();
            this.route.navigate(['login'])
          } else {
            console.log('DAFM error requests response ', JSON.stringify(error));
            const t = this.toastController.create({
              message: this.requestFailed,
              position: 'top',
              duration: 3000,
              cssClass: 'custom-toast-warning',
              buttons: [
                {
                  side: 'start',
                  icon: 'alert-circle-outline',
                  handler: () => {
                    console.log('');
                  },
                },
              ],
            });
            // tslint:disable-next-line: no-shadowed-variable
            t.then((t) => t.present());
            this.loadingController.dismiss();
          }
        });
    }
  }

  async getContexts(hash, owner, type) {
    await this.requestAPI
      .getContexts(hash, this.auth.currentToken)
      .then(response => {
        if (response.status === 200) {
          const parsed = JSON.parse(response.data);
          for (const resp of parsed.content) {
            const responseRequest = resp
            responseRequest.owner = owner;
            responseRequest.requestType = type;
            this.requestAPI.contexts.push(responseRequest);
            if (this.auth.userType !== 'AGENT') {
              this.getReferencePoint(resp.label, resp.hash);
            }
          }
          this.storageService.setLocalData(hash, parsed.content)
        }
      }).catch(error => {
        if (error.status === 401) {
          const t = this.toastController.create({
            message: this.expired,
            position: 'top',
            duration: 3000,
            cssClass: 'custom-toast-warning',
            buttons: [
              {
                side: 'start',
                icon: 'alert-circle-outline',
                handler: () => {
                  console.log('');
                },
              },
            ],
          });
          // tslint:disable-next-line: no-shadowed-variable
          t.then((t) => t.present());
          this.loadingController.dismiss();
          this.route.navigate(['login'])
        } else {
          console.log('DAFM error context response ', JSON.stringify(error));
          const t = this.toastController.create({
            message: this.requestFailed,
            position: 'top',
            duration: 3000,
            cssClass: 'custom-toast-warning',
            buttons: [
              {
                side: 'start',
                icon: 'alert-circle-outline',
                handler: () => {
                  console.log('');
                },
              },
            ],
          });
          // tslint:disable-next-line: no-shadowed-variable
          t.then((t) => t.present());
          this.loadingController.dismiss();
        }
      });
  }

  /**
   * navigate to settings page
   */
  goToSettings() {
    this.route.navigate(['/settings']);
  }

  private getReferencePoint(landParcelLabel: string, hash: string) {
    this.requestAPI.getReferencePoint(landParcelLabel, this.auth.currentToken).then(response => {
      if (response.status === 200) {
        const parsed = JSON.parse(response.data);
        this.storageService.setLocalData(hash + '_point', parsed);
        this.getGeometry(landParcelLabel, hash);
      }
    }).catch((error) => {
      if (error.status === 401) {
        const t = this.toastController.create({
          message: this.expired,
          position: 'top',
          duration: 3000,
          cssClass: 'custom-toast-warning',
          buttons: [
            {
              side: 'start',
              icon: 'alert-circle-outline',
              handler: () => {
                console.log('');
              },
            },
          ],
        });
        // tslint:disable-next-line: no-shadowed-variable
        t.then((t) => t.present());
        this.loadingController.dismiss();
        this.route.navigate(['login'])
      } else {
        console.log('DAFM error response ', JSON.stringify(error));
        const t = this.toastController.create({
          message: this.coordinatedFailed,
          position: 'top',
          duration: 3000,
          cssClass: 'custom-toast-warning',
          buttons: [
            {
              side: 'start',
              icon: 'alert-circle-outline',
              handler: () => {
                console.log('');
              },
            },
          ],
        });
        // tslint:disable-next-line: no-shadowed-variable
        t.then((t) => t.present());
        this.loadingController.dismiss();
      }
    });
  }

  private getGeometry(landParcelLabel: string, hash: string) {
    this.requestAPI.getGeometry(landParcelLabel, this.auth.currentToken).then(response => {
      if (response.status === 200) {
        const parsed = JSON.parse(response.data);
        this.storageService.setLocalData(hash + '_geo', parsed);
      }
    }).catch((error) => {
      if (error.status === 401) {
        const t = this.toastController.create({
          message: this.expired,
          position: 'top',
          duration: 3000,
          cssClass: 'custom-toast-warning',
          buttons: [
            {
              side: 'start',
              icon: 'alert-circle-outline',
              handler: () => {
                console.log('');
              },
            },
          ],
        });
        // tslint:disable-next-line: no-shadowed-variable
        t.then((t) => t.present());
        this.loadingController.dismiss();
        this.route.navigate(['login'])
      } else {
        console.log('DAFM error response ', JSON.stringify(error));
        const t = this.toastController.create({
          message: this.coordinatedFailed,
          position: 'top',
          duration: 3000,
          cssClass: 'custom-toast-warning',
          buttons: [
            {
              side: 'start',
              icon: 'alert-circle-outline',
              handler: () => {
                console.log('');
              },
            },
          ],
        });
        // tslint:disable-next-line: no-shadowed-variable
        t.then((t) => t.present());
        this.loadingController.dismiss();
      }
    });
    // this.loadingController.dismiss();
  }

  getTranslations() {
    this.translate.get('Notifications.Expired').subscribe((value) => {
      this.expired = value;
    });
    this.translate.get('Notifications.RequestsFailed').subscribe((value) => {
      this.requestFailed = value;
    });
    this.translate.get('Notifications.CoordinatesFailed').subscribe((value) => {
      this.coordinatedFailed = value;
    });
  }
}
