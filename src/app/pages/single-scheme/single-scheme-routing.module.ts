import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SingleSchemePage } from './single-scheme.page';

const routes: Routes = [
  {
    path: '',
    component: SingleSchemePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SingleSchemePageRoutingModule {}
