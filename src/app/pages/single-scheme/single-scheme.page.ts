import {
  Component,
  OnInit,
  ViewChild,
  ElementRef,
  OnDestroy,
} from '@angular/core';
import { LocationService } from '../../services/location.service';
import { ActivatedRoute, Router } from '@angular/router';
import {
  AlertController,
  IonBackButtonDelegate,
  ToastController,
} from '@ionic/angular';
import * as Leaflet from 'leaflet';
import * as LeafletOffline from 'leaflet.offline';
import { isPlatform } from '@ionic/angular';
import { ApiService } from 'src/app/services/api.service';
import { ArCameraService } from '../../services/ar-camera.service';
import { urlTemplate } from 'config';
import { TranslateService } from '@ngx-translate/core';
import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { PermissionType, Plugins } from '@capacitor/core';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { SCHEMES } from '../../../../schemes';
const { Permissions } = Plugins;
@Component({
  selector: 'app-single-scheme',
  templateUrl: './single-scheme.page.html',
  styleUrls: ['./single-scheme.page.scss'],
})
export class SingleSchemePage implements OnInit {
  @ViewChild(IonBackButtonDelegate, { static: false })
  backButton: IonBackButtonDelegate;
  alertHeader: any;
  alertSubHeader: any;
  alertButton: any;
  constructor(
    private toastController: ToastController,
    private go: Router,
    public locationService: LocationService,
    public alertController: AlertController,
    private locationAPI: ApiService,
    public arCameraService: ArCameraService,
    private translate: TranslateService,
    private geolocation: Geolocation,
    private locationAccuracy: LocationAccuracy,
    private androidPermissions: AndroidPermissions,
    private diagnostic: Diagnostic,
    private route: ActivatedRoute,
  ) {
    // start watching
    this.locationService.watchLocation();
  }

  ReapMap: Leaflet.Map;
  private tilesDb: any;
  private osmTileLayer;
  private satelliteTileLayer;
  private watchingDevice;

  @ViewChild('ReapMap', { static: false })
  public mapElement: ElementRef;

  longitude: number;
  latitude: number;
  pageTitle: string;
  firstLoad = false;
  stopServices = false;
  deviceLocation = {} as any;
  photoType: any;
  devicePostion: any;
  storedTiles = '';
  saved: string;
  confirmRemoval: string;
  removed: string;
  none: string;
  save: string;
  confirmSaved: string;
  saveExtended: string;
  confirmSavedExtended: string;
  deviceMarker = Leaflet.icon({
    iconUrl: './assets/images/location.svg',
    iconAnchor: [25, 25],
  });
  watchLocationSub: any;
  updateLocation: string;
  permission = false;
  schemeDetails = {} as any;
  /**
   * get notification data
   * get current location
   */
  async ngOnInit() {}

  async ionViewWillEnter() {
    console.log('abcd ',  this.route.snapshot.paramMap.get('id'));
    const scheme =  this.route.snapshot.paramMap.get('id');
    for(const item of SCHEMES.schemes) {
      if(item.owner === scheme) {
        this.schemeDetails = item.details;
      }
    }
    console.log('abcd ', JSON.stringify(this.schemeDetails));
    this.photoType = undefined;
    this.permission = false;
    this.diagnostic.isLocationAvailable().then(async (data) => {
      if (data) {
        this.permission = true;
        this.locationService.startGnss();
        this.firstLoad = true;
        this.stopServices = true;
        await this.watchDeviceLocation();
      } else {
        if (isPlatform('android')) {
          this.locationAccuracy
            .request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY)
            .then(async (location) => {
              if (location.code === 0 || location.code === 1) {
                this.diagnostic.isLocationAvailable().then(async (response) => {
                  if (response) {
                    this.permission = true;
                    this.locationService.startGnss();
                    this.firstLoad = true;
                    this.stopServices = true;
                    await this.watchDeviceLocation();
                  }
                });
              }
            })
            .catch(async (error) => {
              const alert = await this.alertController.create({
                header: this.alertHeader,
                subHeader: this.alertSubHeader,
                buttons: [this.alertButton],
              });
              alert.present();
            });
        }
      }
    });
  }

  ionViewDidEnter() {
    this.getTranslations();
  }

  /**
   * Watch Device Location
   */
  async watchDeviceLocation(bool?: boolean) {
    // subscribe to subject
    // tslint:disable-next-line: deprecation
    this.watchLocationSub = this.geolocation
      .watchPosition({
        enableHighAccuracy: true,
        timeout: 20000,
        // tslint:disable-next-line: deprecation
      })
      .subscribe(
        (position) => {
          this.deviceLocation = position;
          this.latitude = this.deviceLocation.coords.latitude;
          this.longitude = this.deviceLocation.coords.longitude;
          if (this.firstLoad) {
            this.firstLoad = false;
            this.loadMap();
            this.loadDeviceMarker();
          } else {
            this.loadDeviceMarker();
          }
          if (bool) {
            const t = this.toastController.create({
              message: this.updateLocation,
              position: 'top',
              duration: 3000,
              cssClass: 'custom-toast',
              buttons: [
                {
                  side: 'start',
                  icon: 'compass-outline',
                  handler: () => {
                    console.log('');
                  },
                },
              ],
            });
            // tslint:disable-next-line: no-shadowed-variable
            t.then((t) => t.present());
            bool = false;
          }
        },
        (error) => {
          console.log('this is a GPS ERROR', error);
          this.watchDeviceLocation();
        }
      );
  }

  /**
   * once view is loaded
   * run OSM Maps with leaflet
   */
  async loadMap() {
    const t = this;
    // load map
    this.ReapMap = Leaflet.map('ReapMap').setView(
      [this.latitude, this.longitude],
      16
    );
    const baseLayer = Leaflet.tileLayer
      .offline(urlTemplate, {
        minZoom: 15,
        attribution:
          'i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community <a href="http://www.esri.com/">Esri</a>',
      })
      .addTo(this.ReapMap);

    // add save and delete for offline storage
    const offlineControl = Leaflet.control.savetiles(baseLayer, {
      position: 'topright',
      zoomlevels: [13, 16], // optional zoomlevels to save, default current zoomlevel
      confirm(layer, succescallback) {
        // eslint-disable-next-line no-alert
        const tilesAmount = layer._tilesforSave.length;
        if (window.confirm(t.save + tilesAmount + t.saveExtended)) {
          succescallback();
          const toast = t.toastController.create({
            message: t.confirmSaved + tilesAmount + t.confirmSavedExtended,
            duration: 3000,
            position: 'top',
          });
          // tslint:disable-next-line: no-shadowed-variable
          toast.then((toast) => toast.present());
        }
      },

      confirmRemoval(layer, successCallback) {
        let toast: Promise<HTMLIonToastElement>;
        // eslint-disable-next-line no-alert
        if (layer.lengthSaved) {
          if (window.confirm(t.confirmRemoval)) {
            successCallback();
            console.log(layer);
            toast = t.toastController.create({
              message: t.removed,
              duration: 3000,
              position: 'top',
            });
          }
        } else {
          toast = t.toastController.create({
            message: t.none,
            duration: 3000,
            position: 'top',
          });
        }
        // tslint:disable-next-line: no-shadowed-variable
        toast.then((toast) => toast.present());
      },

      saveText:
        '<div class="leaflet-save-button" style="width:30px;height:30px;line-height:40px;font-size:1.8em; background: #004D44; color: white;"><ion-icon name="cloud-download-sharp"></ion-icon></div>',
      rmText:
        '<div class="leaflet-save-button" style="width:30px;height:30px;line-height:40px;font-size:1.8em; background: #f48024; color: white;"><ion-icon name="trash-sharp"></ion-icon></div>',
    });
    offlineControl.addTo(this.ReapMap);

    // get geoJSON
    const getGeoJsonData = () =>
      LeafletOffline.getStorageInfo(urlTemplate).then((data) =>
        LeafletOffline.getStoredTilesAsJson(baseLayer, data)
      );

    // set defaults for icons , fix image bug
    const icon = './assets/images/marker-icon-blue.png';
    const iconShadow = './assets/images/marker-shadow.png';
    const rIcon = './assets/images/marker-icon-2x-blue.png';
    const DefaultIcon = Leaflet.icon({
      iconUrl: icon,
      iconSize: [25, 41],
      iconRetinaUrl: rIcon,
      shadowUrl: iconShadow,
      iconAnchor: [12, 41],
      popupAnchor: [-1, -36],
    });
    Leaflet.Marker.prototype.options.icon = DefaultIcon;
    // center on the marker
    this.ReapMap.panTo(new Leaflet.LatLng(this.latitude, this.longitude));
    // create data for info popup
  }
  /**
   *  get device location and create new Marker object
   */
  loadDeviceMarker(): void {
    // initialise marker
    if (!this.devicePostion) {
      this.devicePostion = Leaflet.marker(
        [
          this.deviceLocation.coords.latitude,
          this.deviceLocation.coords.longitude,
        ],
        { icon: this.deviceMarker }
      );
      this.devicePostion.addTo(this.ReapMap);
    } else {
      // update marker position
      // prevents multiple markers being created
      this.devicePostion.setLatLng([
        this.deviceLocation.coords.latitude,
        this.deviceLocation.coords.longitude,
      ]);
    }
  }

  /**
   * Go to Camera Page
   * @param id hash value
   */
  goToCamera() {
    const t = this;
    this.stopServices = false;
    this.locationAccuracy
      .request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY)
      .then((data) => {
        this.locationService.contain = true;
        this.go.navigate(['/ar-camera/' + this.schemeDetails.owner]);
      })
      .catch(async (error) => {
        const alert = await this.alertController.create({
          header: this.alertHeader,
          subHeader: this.alertSubHeader,
          buttons: [this.alertButton],
        });
        alert.present();
      });
  }

  selectPhotoType() {
    console.log(this.photoType);
    const params = {
      owner: this.schemeDetails.owner,
      type: this.schemeDetails.requestType
    }
    console.log('abcd ', JSON.stringify(params));
    this.locationService.photoDetails.scheme = this.schemeDetails.owner;
    this.locationService.photoDetails.requestParams = params;
    // setting parcel ID to photo type to overlay images (quick fix)
    this.locationService.photoDetails.parcelId = this.photoType;
    this.locationService.photoDetails.phototype = this.photoType;
    this.locationService.photoDetails.isInField = 'N/A';
  }

  ionViewDidLeave(): void {
    if (this.stopServices) {
      this.locationService.stopGnss();
      this.locationService.stopWatch();
      this.watchLocationSub.unsubscribe();
      this.devicePostion = null;
      // remove map
      this.ReapMap.off();
      this.ReapMap.remove();
      this.ReapMap.stop();
    } else {
      this.watchLocationSub.unsubscribe();
      this.devicePostion = null;
      // remove map
      this.ReapMap.off();
      this.ReapMap.remove();
      this.ReapMap.stop();
    }
  }

  private getTranslations() {
    // tslint:disable-next-line: deprecation
    this.translate.get('Map.Saved').subscribe((value) => {
      this.saved = value;
    });
    // tslint:disable-next-line: deprecation
    this.translate.get('Map.ConfirmRemoval').subscribe((value) => {
      this.confirmRemoval = value;
    });
    // tslint:disable-next-line: deprecation
    this.translate.get('Map.Removed').subscribe((value) => {
      this.removed = value;
    });
    // tslint:disable-next-line: deprecation
    this.translate.get('Map.None').subscribe((value) => {
      this.none = value;
    });
    // tslint:disable-next-line: deprecation
    this.translate.get('Map.Save').subscribe((value) => {
      this.save = value;
    });
    // tslint:disable-next-line: deprecation
    this.translate.get('Map.ConfirmSaved').subscribe((value) => {
      this.confirmSaved = value;
    });
    // tslint:disable-next-line: deprecation
    this.translate.get('Map.SaveExtended').subscribe((value) => {
      this.saveExtended = value;
    });
    // tslint:disable-next-line: deprecation
    this.translate.get('Map.ConfirmSavedExtended').subscribe((value) => {
      this.confirmSavedExtended = value;
    });

    this.translate.get('Map.UpdateLocation').subscribe((value) => {
      this.updateLocation = value;
    });
    this.translate.get('Map.AlertHeader').subscribe((value) => {
      this.alertHeader = value;
    });
    this.translate.get('Map.AlertSubHeader').subscribe((value) => {
      this.alertSubHeader = value;
    });
    this.translate.get('Map.AlertButton').subscribe((value) => {
      this.alertButton = value;
    });
  }
}
