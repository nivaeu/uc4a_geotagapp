import { Injectable } from '@angular/core';
import { Media, MediaObject } from '@ionic-native/media/ngx';
import { isPlatform } from '@ionic/angular';
import { beep, landParcelAudio, landParcelsAudio, loginAudio, photosAudio, requestsAudio } from 'config';
@Injectable({
  providedIn: 'root'
})
export class AudioService {
  loginAudioFile: MediaObject;
  requestsAudioFile: MediaObject;
  photosAudioFile: MediaObject;
  landParcelsAudioFile: MediaObject;
  landParcelPageAudioFile: MediaObject;
  beepAudioFile: MediaObject;
  requestPlaying: boolean;
  constructor(private media: Media) { }

  getRequestAudioFile(){
    if (isPlatform('android')) {
      const path = `/android_asset/public/assets/audio/${requestsAudio}.mp3`
      this.requestsAudioFile = this.media.create(path);
    } else if (isPlatform('ios')) {
      const path = `assets/audio/${requestsAudio}.mp3`;
      this.requestsAudioFile = this.media.create(path);
    } else {
      return;
    }
  }

  getPhotosAudioFile(){
    if (isPlatform('android')) {
      const path = `/android_asset/public/assets/audio/${photosAudio}.mp3`
      this.photosAudioFile = this.media.create(path);
    } else if (isPlatform('ios')) {
      const path = `assets/audio/${photosAudio}.mp3`;
      this.photosAudioFile = this.media.create(path);
    } else {
      return;
    }
  }

  getLoginAudioFile(){
    if (isPlatform('android')) {
      const path = `/android_asset/public/assets/audio/${loginAudio}.mp3`
      this.loginAudioFile = this.media.create(path);
    } else if (isPlatform('ios')) {
      const path = `assets/audio/${loginAudio}.mp3`;
      this.loginAudioFile = this.media.create(path);
    } else {
      return;
    }
  }

  getLandParcelsAudioFile(){
    if (isPlatform('android')) {
      const path = `/android_asset/public/assets/audio/${landParcelsAudio}.mp3`
      this.landParcelsAudioFile = this.media.create(path);
    } else if (isPlatform('ios')) {
      const path = `assets/audio/${landParcelsAudio}.mp3`;
      this.landParcelsAudioFile = this.media.create(path);
    } else {
      return;
    }
  }

  getLandParcelPageAudioFile(){
    if (isPlatform('android')) {
      const path = `/android_asset/public/assets/audio/${landParcelAudio}.mp3`
      this.landParcelPageAudioFile = this.media.create(path);
    } else if (isPlatform('ios')) {
      const path = `assets/audio/${landParcelAudio}.mp3`;
      this.landParcelPageAudioFile = this.media.create(path);
    } else {
      return;
    }
  }

  getBeepAudioFile(){
    if (isPlatform('android')) {
      const path = `/android_asset/public/assets/audio/${beep}.mp3`
      this.beepAudioFile = this.media.create(path);
    } else if (isPlatform('ios')) {
      const path = `assets/audio/${beep}.mp3`;
      this.beepAudioFile = this.media.create(path);
    } else {
      return;
    }
  }
}
