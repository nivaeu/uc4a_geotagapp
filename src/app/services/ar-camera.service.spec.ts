import { TestBed } from '@angular/core/testing';

import { ArCameraService } from './ar-camera.service';

describe('ArCameraService', () => {
  let service: ArCameraService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ArCameraService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
