import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { AuthenticationService } from 'src/app/services/authentication.service';
import {
  bearer, contexts, geometry, geometryUrl, getContextsUrl,
  getRequestsUrl, landAppId, landAppKey, postLandLocationsUrl, postResponseUrl, referencePoint,
  referencePointuUrl, requestAppId, requestAppKey, getAgentRequests, postResponsev1
} from '../../../config'
// import randomString from 'randomstring';
@Injectable({
  providedIn: 'root',
})
export class ApiService {
  public requests = Array<IRequest>();
  public contexts = [] as IRequestContext[];
  public referencePoint = {} as IReferencePoint;
  public geoCoordinates = {} as IGeoCoordinates;
  public selectedParcel = {} as IParcel;
  public random: string;

  constructor(private http: HTTP, private auth: AuthenticationService) { }

  getRequests(token: string) {

    let requestUrl = '';

    if (this.auth.userType === 'AGENT') {
      requestUrl = getAgentRequests
    }
    else {
      requestUrl = getRequestsUrl
    }
    const params = {};
    const header = {
      app_id: requestAppId,
      app_key: requestAppKey,
      Authorization: bearer + token,
    };
    return this.http.get(requestUrl, params, header);
  }

  getContexts(hash: string, token: string) {
    const url = getContextsUrl + hash + contexts;
    const params = {};
    const header = {
      app_id: requestAppId,
      app_key: requestAppKey,
      Authorization: bearer + token,
    };
    return this.http.get(url, params, header);
  }

  getReferencePoint(landParcelLabel: string, token: string) {
    const url = referencePointuUrl + landParcelLabel + referencePoint;
    const params = {};
    const header = {
      app_id: landAppId,
      app_key: landAppKey,
      Authorization: bearer + token,
    };
    return this.http.get(url, params, header);
  }

  getGeometry(landParcelLabel: string, token: string) {
    const url = geometryUrl + landParcelLabel + geometry;
    const params = {};
    const header = {
      app_id: landAppId,
      app_key: landAppKey,
      Authorization: bearer + token,
    };
    return this.http.get(url, params, header);
  }

  postLandLocations(token: string, latitude: number, longitude: number) {
    const body = {
      radius: 1000,
      shape: {
        type: 'Point',
        typeName: 'Point',
        coordinates: [longitude, latitude],
      },
    };
    const header = {
      app_id: landAppId,
      app_key: landAppKey,
      Authorization: bearer + token,
    };
    this.http.setDataSerializer('json');
    return this.http.post(postLandLocationsUrl, body, header);
  }

  postResponse(
    image: any,
    requestReference: string,
    name: string,
    data: string,
    token: string
  ) {
    let url = '';
    if (requestReference === 'REAP' || requestReference === 'undefined' || requestReference === 'FES' || requestReference === 'AECM') {
      // url = postResponseUrl
      url = postResponsev1
    }
    else {
      url = postResponseUrl + '/' + requestReference;
    }

    const formData = new FormData();
    // append file to formdata
    formData.append('file', image, name);
    formData.append('data', data);
    // set to multipart to support files
    this.http.setDataSerializer('multipart');
    const header = {
      app_id: requestAppId,
      app_key: requestAppKey,
      Authorization: bearer + token,
    };
    return this.http.post(url, formData, header);
  }

  randomGenerator(){
    this.random = 'X' + Math.floor(100000 + Math.random() * 900000);
  }
}

interface IRequest {
  id?: number;
  type?: string;
  geotag?: string;
  herdNumber?: string;
  year?: number;
  correspondenceId?: number;
  correspondenceDocNo?: number;
  hash?: string;
  status?: string;
  owner?: string;
  workflowId?: string;
  context?: IRequestContext[];
}

interface IRequestContext {
  type?: string;
  label?: string;
  comment?: string;
  hash?: string;
  status?: string;
  owner?: string;
  requestType?: string;
}

interface IReferencePoint {
  id: number;
  label: string;
  referencePoint: {
    type: string;
    typeName: string;
    coordinates: any[];
  };
}

interface IGeoCoordinates {
  id: number;
  label: string;
  geoCoordinates: IGeo;
}

interface IGeo {
  type: string;
  typeName: string;
  coordinates: any[];
}
interface ICurrentLocation {
  latitude?: number;
  longitude?: number;
}
interface IParcel {
  polygon: any;
  label: string;
  marker: any;
  referencePoint: any;
  scheme: string;
  type: string;
}
