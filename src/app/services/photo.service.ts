import { Injectable } from '@angular/core';
import {
  Plugins, CameraResultType, Capacitor, FilesystemDirectory,
  CameraPhoto, CameraSource
} from '@capacitor/core';
import { Platform } from '@ionic/angular';
import { PHOTO_STORAGE } from 'config';
import { LocationService } from './location.service';
import { isPlatform } from '@ionic/angular';
import { Device } from '@ionic-native/device/ngx';
const { Camera, Filesystem, Storage } = Plugins;

@Injectable({
  providedIn: 'root'
})

export class PhotoService {
  public photos: Photo[] = [];
  public selectedPhotos: Photo[] = [];
  public galleryPhotos: Photo[] = [];
  private platform: Platform;
  public base64String = '';

  constructor(platform: Platform, public locationService: LocationService, private device: Device) {
    this.platform = platform;
  }

  private async savePicture(cameraPhoto: any, details: PictureDetails) {
    // Convert photo to base64 format, required by Filesystem API to save
    // const base64Data = await this.readAsBase64(cameraPhoto);

    // Write the file to the data directory
    const fileName = new Date().getTime() + '.jpeg';
    details.name = fileName;
    await Filesystem.writeFile({
      path: fileName,
      data: cameraPhoto,
      directory: FilesystemDirectory.Data,
    });

    // Get platform-specific photo filepaths
    return await this.getPhotoFile(cameraPhoto, fileName, details);
  }

  public async addNewToGallery(result: any) {
    const base64PictureData = result.value;
    // Take a photo
    /* const capturedPhoto = await Camera.getPhoto({
      resultType: CameraResultType.Uri, // file-based data; provides best performance
      source: CameraSource.Camera, // automatically take a new photo with the camera
      quality: 80 // highest quality (0 to 100)
    }); */

    this.locationService.photoDetails.sent = false;
    this.locationService.getLocation();
    this.locationService.getGyro();
    this.locationService.getDirection();
    this.getDeviceDetails();
    // Telephony information is only available on android
    if (this.platform.is('android')) {
      // get cell tower and network information
      this.locationService.getNetworkDetails();
    }
    // Save the picture and add it to photo collection
    const savedImageFile = await this.savePicture(base64PictureData, this.locationService.photoDetails);
    this.photos.unshift(savedImageFile);
    this.locationService.stopGnss();
    this.locationService.stopWatch();

    Storage.set({
      key: PHOTO_STORAGE,
      value: this.platform.is('hybrid')
        ? JSON.stringify(this.photos)
        : JSON.stringify(this.photos.map(p => {
          // Don't save the base64 representation of the photo data,
          // since it's already saved on the Filesystem
          const photoCopy = { ...p };
          // delete photoCopy.base64;
          return photoCopy;
        }))
    });
  }

  private async readAsBase64(cameraPhoto: CameraPhoto) {
    // "hybrid" will detect Cordova or Capacitor
    if (this.platform.is('hybrid')) {
      // Read the file into base64 format
      const file = await Filesystem.readFile({
        path: cameraPhoto.path
      });
      this.base64String = file.data;
      return file.data;
    } else {
      // Fetch the photo, read as a blob, then convert to base64 format
      const response = await fetch(cameraPhoto.webPath);
      const blob = await response.blob();

      return await this.convertBlobToBase64(blob) as string;
    }
  }

  convertBlobToBase64 = (blob: Blob) => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onerror = reject;
    reader.onload = () => {
      resolve(reader.result);
    };
    reader.readAsDataURL(blob);
  })

  private async getPhotoFile(cameraPhoto, fileName, details) {
    if (this.platform.is('hybrid')) {
      // Get the new, complete filepath of the photo saved on filesystem
      const fileUri = await Filesystem.getUri({
        directory: FilesystemDirectory.Data,
        path: fileName
      });

      // Display the new image by rewriting the 'file://' path to HTTP
      // Details: https://ionicframework.com/docs/building/webview#file-protocol
      return {
        filepath: fileUri.uri,
        webviewPath: Capacitor.convertFileSrc(fileUri.uri),
        pictureDetails: details
      };
    } else {
      // Use webPath to display the new image instead of base64 since it's
      // already loaded into memory
      return {
        filepath: fileName,
        webviewPath: cameraPhoto.webPath,
        pictureDetails: details
      };
    }
  }

  public async loadSaved() {
    // Retrieve cached photo array data
    const photos = await Storage.get({ key: PHOTO_STORAGE });
    this.photos = JSON.parse(photos.value) || [];
    if (this.photos.length > 0) {
      this.photos.forEach(async photo => {
        // check if scheme is present
        if (!photo.pictureDetails.hasOwnProperty('scheme')) {
          // if no scheme set a default
          photo.pictureDetails.scheme = 'no scheme';
        }
        photo.selected = false;
        // overwrite webview path as it changs with updates to iOS
        if (isPlatform('ios')) {
          await Filesystem.getUri({
            directory: FilesystemDirectory.Documents,
            path: photo.pictureDetails.name
          }).then((result) => {

            photo.filepath = result.uri.replace('file://', 'file://');
            photo.webviewPath = result.uri.replace('file://', 'capacitor://localhost/_capacitor_file_');
          }, (err) => {
            console.log('read file ERROR', err)
          });
        }
      });
    }
    console.log(JSON.stringify(this.photos));
    this.galleryPhotos = [...this.photos];
    // Easiest way to detect when running on the web:
    // “when the platform is NOT hybrid, do this”
    if (!this.platform.is('hybrid')) {
      // Display the photo by reading into base64 format
      for (const photo of this.photos) {
        // Read each saved photo's data from the Filesystem
        const readFile = await Filesystem.readFile({
          path: photo.filepath,
          directory: FilesystemDirectory.Data
        });

        // Web platform only: Save the photo into the base64 field
        photo.base64 = `data:image/jpeg;base64,${readFile.data}`;
      }
    }
  }

  public async deletePicture(photo: Photo, position: number) {
    // Remove this photo from the Photos reference data array
    this.cancelPicture(photo, position);
    console.log(this.photos);
    // remove from gallery photos
    this.galleryPhotos.splice(position, 1);
    // remove form main photos array
    this.photos = this.photos.filter((item) => {
      return item !== photo
    });
    console.log(this.photos);
    // Update photos array cache by overwriting the existing photo array
    Storage.set({
      key: PHOTO_STORAGE,
      value: JSON.stringify(this.photos)
    });

    // delete photo file from filesystem
    const filename = photo.filepath
      .substr(photo.filepath.lastIndexOf('/') + 1);

    await Filesystem.deleteFile({
      path: filename,
      directory: FilesystemDirectory.Data
    });
  }

  public async batchDelete() {
    for (const photo of this.selectedPhotos) {
      this.galleryPhotos = this.galleryPhotos.filter((item) => {
        return item !== photo
      });
      this.photos = this.photos.filter((item) => {
        return item !== photo
      });
      const filename = photo.filepath
      .substr(photo.filepath.lastIndexOf('/') + 1);

      await Filesystem.deleteFile({
        path: filename,
        directory: FilesystemDirectory.Data
      });
    }
    Storage.set({
      key: PHOTO_STORAGE,
      value: JSON.stringify(this.photos)
    });
    this.deselectPhotos();
  }

  /* /**
  * Select Photo
  * @return selected image Array
  */

  public async selectPicture(photo: Photo, position: number) {
    // get selected image
    const selected = this.galleryPhotos[position];
    if (this.galleryPhotos[position].selected === true) {
      this.cancelPicture(photo, position);
      return;
    }
    else {
      this.galleryPhotos[position].selected = true;
      // console.log(this.photos);
      // push to array
      if (this.selectedPhotos.indexOf(selected) === -1) {
        this.selectedPhotos.push(selected);
      }
    }
  }
  public async markAsSent(photoName: string) {
    // get selected image
    const obj = this.galleryPhotos.find(image => image.pictureDetails.name === photoName);
    const index = this.galleryPhotos.indexOf(obj);
    this.galleryPhotos[index].pictureDetails.sent = true;
    // Update photos array cache by overwriting the existing photo array
    Storage.set({
      key: PHOTO_STORAGE,
      value: JSON.stringify(this.photos)
    });
  }

  public async cancelPicture(photo: Photo, position: number) {
    // let cancelled = this.photos[position];
    // set selection to false
    this.galleryPhotos[position].selected = false;
    // remove from selected array
    const index = this.selectedPhotos.indexOf(photo);
    if (index !== -1) {
      this.selectedPhotos.splice(index, 1);
    }
  }

  public async deselectPhotos() {
    this.selectedPhotos = [];
    this.galleryPhotos.forEach(photo => {
      photo.selected = false;
    });
  }

  public async base64toBlob(photo: Photo): Promise<Blob> {
    const fileUri = await Filesystem.getUri({
      directory: FilesystemDirectory.Data,
      path: photo.filepath
    });

    const converted = Capacitor.convertFileSrc(fileUri.uri);
    const readFile = await Filesystem.readFile({
      path: converted,
      // directory: FilesystemDirectory.Data
    });

    const base64 = `data:image/jpeg;base64,${readFile.data}`;

    const base64Response = await fetch(base64);
    return await base64Response.blob() as Blob;
  }

  getDeviceDetails() {
    const deviceDetails = {
      manufacturer: this.device.manufacturer,
      platform: this.device.platform,
      version: this.device.version
    }
    this.locationService.photoDetails.device = deviceDetails;
  }
}

// photo interface
interface Photo {
  filepath: string;
  webviewPath: string;
  pictureDetails?: PictureDetails;
  base64?: string;
  selected?: boolean;
}

interface PictureDetails {
  requestParams: {
    label?: string;
    owner?: string;
    type?: string;
  }
  phototype?: string;
  scheme?: string;
  name: string;
  latitude?: number;
  longitude?: number;
  direction?: string;
  gyro_x?: number;
  gyro_y?: number;
  gyro_z?: number;
  isInField?: string;
  distanceFromMarker?: number;
  parcelId?: string;
  requestId?: string;
  sent?: boolean;
  ccsId?: string;
  device?: {
    manufacturer?: string;
    platform?: string;
    version?: string;
  };
  timeStamp: Date;
  isGal?: boolean;
  isBei?: boolean;
  isGlo?: boolean;
  isGps?: boolean;
  isQzss?: boolean;
  isSbas?: boolean;
  isDual?: boolean;
  isOsnma?: boolean;
  networkDetails?: NetworkDetails;
  cellTower?: CellTower
}

interface NetworkDetails {
  wifi?: {
    ssid?: string;
    bssid?: string;
    frequency?: number;
    channel?: number;
    signal?: number;
  };
  mcc?: number;
  mnc?: number;
  netOp?: string;
  lac?: number;
  cid?: number;
  radio?: string;
  radio_int?: string;
}
interface CellTower {
  status?: string;
  balance?: number;
  lat?: any;
  lon?: any;
  accuracy?: number;
  address?: string;
  distance?: number;
}