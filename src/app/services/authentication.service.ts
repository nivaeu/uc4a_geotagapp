import { Platform } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { StorageService } from './storage.service';
import { NetworkService } from './network.service';
import jwt_decode from 'jwt-decode';
import { HTTP } from '@ionic-native/http/ngx';
import {
  appId, logoutUrl, headers, TOKEN_KEY, authorizationBaseUrl, accessTokenEndpoint, scope,
  resourceUrl, redirectUrl, responseType, windowOptions, webRedirectUrl, acceptedTerms
} from '../../../config'
import { Plugins } from '@capacitor/core';
@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  acceptTerms = {
    value: 'accepted',
    isChecked: false,
  }
  currentToken: string;
  userType: string;
  constructor(
    private plt: Platform,
    private storageService: StorageService,
    private networkService: NetworkService,
    private http: HTTP,
  ) {
    this.plt.ready().then(() => {
      this.checkToken();
    });
  }
  authenticationState = new BehaviorSubject(false);

  checkToken() {
    this.storageService.getLocalData(TOKEN_KEY).then((res) => {
      if (res === null) {
        this.authenticationState.next(false);
      } else {
        this.validateToken(res);
      }
    });
  }

  login(token: string) {
    return this.storageService.setLocalData(TOKEN_KEY, token).then(() => {
      this.validateToken(token);
    });
  }

  logout() {
    this.storageService.getLocalData(TOKEN_KEY).then(async (token) => {
      const params = {
        client_id: appId,
        id_token_hint: token
      };
      await this.http.get(logoutUrl, params, headers).then((response) => {
        if (response.status === 200) {
          this.storageService.removeLocalData(TOKEN_KEY).then(() => {
            this.authenticationState.next(false);
          });
        } else {
          this.storageService.removeLocalData(TOKEN_KEY).then(() => {
            this.authenticationState.next(false);
          });
        }
      });
    });

  }

  isAuthenticated() {
    return this.authenticationState.value;
  }

  private validateToken(token: any) {
    if (this.networkService.getCurrentNetworkStatus() === false && token) {
      const decodedToken = jwt_decode(token);
      this.userType = decodedToken.account_type;
      this.authenticationState.next(true)
    } else {
      this.decodeToken(token);
    }
  }

  private decodeToken(token: string) {
    const decodedToken = jwt_decode(token);
    if (Date.now() < decodedToken.exp * 1000) {
      this.authenticationState.next(true);
      this.currentToken = token;
      this.userType = decodedToken.account_type;
      console.log('Dave user type', this.userType);
    } else {
      this.authenticationState.next(false);
    }
  }

  async authenticate(acceptTerms: boolean){
    try {
      const resourceUrlResponse = await Plugins.OAuth2Client.authenticate({
        authorizationBaseUrl,
        accessTokenEndpoint,
        scope,
        resourceUrl,
        web: {
          appId,
          responseType,
          redirectUrl: webRedirectUrl,
          windowOptions
        },
        android: {
          appId,
          responseType,
          redirectUrl
        },
        ios: {
          appId,
          responseType,
          redirectUrl
        }
      });
      this.login(resourceUrlResponse.access_token);
      this.acceptTerms.isChecked = acceptTerms;
      this.storageService.setLocalData(acceptedTerms, JSON.stringify(this.acceptTerms))
    } catch (err) {
      console.error(err);
    }
  }
}