import { Injectable } from '@angular/core';
import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';
import {
  Gyroscope,
  GyroscopeOrientation,
  GyroscopeOptions,
} from '@ionic-native/gyroscope/ngx';
import {
  DeviceOrientation,
  DeviceOrientationCompassHeading,
} from '@ionic-native/device-orientation/ngx';
import { PermissionType, Plugins } from '@capacitor/core';
import {
  registerReadCallback,
  stopGnssStatus,
} from '../../../plugins/gnss_status_plugin/www/gnss_status.js';
import { isPlatform } from '@ionic/angular';
import { getNetworkInfo } from '../../../plugins/cell_info_plugin/www/cell_info.js';
import { HTTP } from '@ionic-native/http/ngx';
// import { HttpClient, HttpHeaders } from "@angular/common/http";
import { cellTowerAPI, cellTowerHeaders, cellTowerUrl } from 'config';
import { Subject } from 'rxjs';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';

const { Storage, Permissions } = Plugins;

@Injectable({
  providedIn: 'root',
})
export class LocationService {

  constructor(
    private geolocation: Geolocation,
    private gyroscope: Gyroscope,
    private deviceOrientation: DeviceOrientation,
    private http: HTTP,
    private location: LocationAccuracy
  ) { }
  /**
   * Geolocation data
   */
  public _Location = new Subject<any>();

  public photoDetails = {} as PictureDetails;

  public networkDetails = {} as NetworkDetails;

  public cellTower = {} as CellTower;

  public watchLocationSub: any;

  public contain = false;
  /**
   * Get current geolocation data
   */
  public async getLocation() {
    // const data = getNetworkInfo();
    const posOptions = {
      enableHighAccuracy: true,
      timeout: 10000,
      maximumAge: 30000,
    };
    this.geolocation
      .getCurrentPosition(posOptions)
      .then((resp) => {
        this.photoDetails.latitude = resp.coords.latitude;
        this.photoDetails.longitude = resp.coords.longitude;
      })
      .catch((error) => {
        console.log('Error getting location', error);
      });
    this.getTimeStamp();
  }
  /**
   * watch Location
   *
   */
  public async watchLocation() {
    this.watchLocationSub = this.geolocation.watchPosition({
      enableHighAccuracy: true,
      timeout: 20000
      // tslint:disable-next-line: deprecation
    }).subscribe((position) => {

      this._Location.next(position);

    }, (err) => {
      console.log('error from watch service', err);
    })
    return this.watchLocationSub;
  }
  /**
   * get gyro (x,y,z) data
   */
  public async getGyro() {
    const options: GyroscopeOptions = {
      frequency: 3000,
    };

    this.gyroscope
      .getCurrent(options)
      .then((orientation: GyroscopeOrientation) => {
        this.photoDetails.gyro_x = orientation.x;
        this.photoDetails.gyro_y = orientation.y;
        this.photoDetails.gyro_z = orientation.z;
      })
      .catch((error) => {
        // console.log('Error getting gyro', error);
      });
  }
  /**
   * get device Direction / Compass heading
   */
  public async getDirection() {
    // Get the device current compass heading
    this.deviceOrientation
      .getCurrentHeading()
      .then((data: DeviceOrientationCompassHeading) => {
        let compassValue = '';
        const direction: number = data.magneticHeading;
        if (direction > 23 && direction <= 67) {
          compassValue = 'NE - ' + direction + ' degrees';
        } else if (direction > 68 && direction <= 112) {
          compassValue = 'E - ' + direction + ' degrees';
        } else if (direction > 113 && direction <= 167) {
          compassValue = 'SE - ' + direction + ' degrees';
        } else if (direction > 168 && direction <= 202) {
          compassValue = 'S - ' + direction + ' degrees';
        } else if (direction > 203 && direction <= 247) {
          compassValue = 'SW - ' + direction + ' degrees';
        } else if (direction > 248 && direction <= 293) {
          compassValue = 'W - ' + direction + ' degrees';
        } else if (direction > 294 && direction <= 337) {
          compassValue = 'NW - ' + direction + ' degrees';
        } else if (direction >= 338 || direction <= 22) {
          compassValue = 'N - ' + direction + ' degrees';
        }
        this.photoDetails.direction = compassValue;
      })
      .catch((error) => {
        // console.log(error);
      });
  }
  public async getTimeStamp() {
    this.photoDetails.timeStamp = new Date();
  }

  public startGnss() {
    const t = this;
    if (isPlatform('android')) {
      registerReadCallback(
        function success(data) {
          t.readInternalGnssInfo(data);
        },
        // error attaching the callback
        function error(err) {
          console.log('ERROR ATTACHING CALLBACK - ' + err);
        }
      );
    }
  }

  public stopWatch() {
    console.log('Stop watch called');
    // this._Location.unsubscribe();
    this.watchLocationSub.unsubscribe();
  }

  public stopGnss() {
    if (isPlatform('android')) {
      stopGnssStatus(
        function success(data) {
          console.log('GNSS Stopped');
        },
        function error(err) {
          console.log('ERROR STOPPING: ' + err);
        }
      );
    }
  }

  readInternalGnssInfo(data) {
    const array = new Uint8Array(data);
    const arr = new TextDecoder().decode(array);
    if (array.length >= 1) {
      // tslint:disable-next-line: prefer-for-of
      for (let i = 0; i < array.length; i++) {
        if (array[i] === 13) {
          const parsed = JSON.parse(arr);
          try {
            if (parsed.type === 'gnss_status') {
              if (parsed.constellations.indexOf('1') > -1) {
                this.photoDetails.isGps = true;
              } else {
                this.photoDetails.isGps = false;
              }
              if (parsed.constellations.indexOf('2') > -1) {
                this.photoDetails.isSbas = true;
              } else {
                this.photoDetails.isSbas = false;
              }
              if (parsed.constellations.indexOf('3') > -1) {
                this.photoDetails.isGlo = true;
              } else {
                this.photoDetails.isGlo = false;
              }
              if (parsed.constellations.indexOf('4') > -1) {
                this.photoDetails.isQzss = true;
              } else {
                this.photoDetails.isQzss = false;
              }
              if (parsed.constellations.indexOf('5') > -1) {
                this.photoDetails.isBei = true;
              } else {
                this.photoDetails.isBei = false;
              }
              if (parsed.constellations.indexOf('6') > -1) {
                this.photoDetails.isGal = true;
              } else {
                this.photoDetails.isGal = false;
              }
            }
            if (parsed.type === 'dual_freq') {
              const dualFreq = parsed.value;
              if (dualFreq) {
                this.photoDetails.isDual = true;
              } else {
                this.photoDetails.isDual = false;
              }
            } else {
              this.photoDetails.isDual = false;
            }
            if (parsed.type === 'osnma') {
              const osnma = parsed.value;
              if (osnma) {
                this.photoDetails.isOsnma = true;
              } else {
                this.photoDetails.isOsnma = false;
              }
            } else {
              this.photoDetails.isOsnma = false;
            }
          } catch (e) {
            console.log('ERR: ' + e);
          }
        }
      }
    }
  }
  /**
   * Get cell tower and network connection information
   */
  public getNetworkDetails() {
    const t = this;
    // from eGNNS plugin
    getNetworkInfo(
      function success(data) {
        t.readNetworkData(data);
      },
      function error(err) {
        console.log('There was an error returning network data ', err);
      }
    );
  }
  private readNetworkData(data) {
    this.networkDetails = data;
    this.photoDetails.networkDetails = this.networkDetails;
    this.getCellTowerData();
  }

  public getCellTowerData() {
    const params = {
      token: cellTowerAPI,
      radio: this.photoDetails.networkDetails.radio,
      mcc: this.photoDetails.networkDetails.mcc,
      mnc: this.photoDetails.networkDetails.mnc,
      cells: [
        {
          lac: this.photoDetails.networkDetails.lac,
          cid: this.photoDetails.networkDetails.cid,
        },
      ],
      address: 1,
    };
    this.http.setDataSerializer('json');
    this.http.post(cellTowerUrl, params, cellTowerHeaders).then((response) => {
      if (response.status === 200) {
        this.photoDetails.cellTower = JSON.parse(response.data);
        const distance: number = this.HaversineFormula(
          this.photoDetails.latitude,
          this.photoDetails.cellTower.lat,
          this.photoDetails.longitude,
          this.photoDetails.cellTower.lon
        );
        this.photoDetails.cellTower.distance =
          Math.round((distance + Number.EPSILON) * 100) / 100;
      }
    });
  }

  HaversineFormula(lat1: number, lat2: number, lng1: number, lng2: number) {
    const R = 6371e3; // metres
    const φ1 = (lat1 * Math.PI) / 180; // φ, λ in radians
    const φ2 = (lat2 * Math.PI) / 180;
    const Δφ = ((lat2 - lat1) * Math.PI) / 180;
    const Δλ = ((lng2 - lng1) * Math.PI) / 180;

    const a =
      Math.sin(Δφ / 2) * Math.sin(Δφ / 2) +
      Math.cos(φ1) * Math.cos(φ2) * Math.sin(Δλ / 2) * Math.sin(Δλ / 2);
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    return R * c; // in metres
  }
}

interface PictureDetails {
  requestParams: {
    label?: string;
    owner?: string;
    type?: string;
  }
  phototype?: string;
  scheme?: string;
  name: string;
  latitude?: number;
  longitude?: number;
  direction?: string;
  gyro_x?: number;
  gyro_y?: number;
  gyro_z?: number;
  isInField?: string;
  distanceFromMarker?: number;
  parcelId?: string;
  requestId?: string;
  sent?: boolean;
  ccsId?: string;
  device?: {
    manufacturer?: string;
    platform?: string;
    version?: string;
  };
  timeStamp: Date;
  isGal?: boolean;
  isBei?: boolean;
  isGlo?: boolean;
  isGps?: boolean;
  isQzss?: boolean;
  isSbas?: boolean;
  isDual?: boolean;
  isOsnma?: boolean;
  networkDetails?: NetworkDetails;
  cellTower?: CellTower
}

interface NetworkDetails {
  wifi?: {
    ssid?: string;
    bssid?: string;
    frequency?: number;
    channel?: number;
    signal?: number;
  };
  mcc?: number;
  mnc?: number;
  netOp?: string;
  lac?: number;
  cid?: number;
  radio?: string;
  radio_int?: string;
  // cellTower?: CellTower;
}
interface CellTower {
  status?: string;
  balance?: number;
  lat?: any;
  lon?: any;
  accuracy?: number;
  address?: string;
  distance?: number;
}
