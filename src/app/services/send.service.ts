import { Injectable } from '@angular/core';
import { Platform, ToastController } from '@ionic/angular';
import { HTTP } from '@ionic-native/http/ngx';
import { PhotoService } from './photo.service';
import { File, FileEntry } from '@ionic-native/File/ngx';
import { ApiService } from './api.service';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
@Injectable({
  providedIn: 'root'
})
export class SendService {
  private platform: Platform;
  success: string;
  expired: string;
  failed: string;
  constructor(
    private http: HTTP,
    public photoService: PhotoService,
    private toast: ToastController,
    private file: File,
    private sendAPI: ApiService,
    private route: Router,
    private translate: TranslateService
  ) { }


  getFileBlob(filepath: string, requestid: string, name: string, data: string, token: string) {
    // convert the file
    this.getSingleFile(filepath)
      .then(fileBlob => {
        this.sendData(fileBlob, requestid, name, data, token)
      })
      .catch(err => {
        console.log('Error while reading file.');
      });
  }

  /**
   * send data to API
   * image file needs to be JS File
   *
   * @param image : File
   * @param hash : string
   * @param name : string
   */
  public async sendData(image: any, hash: string, name: string, data: string, token: string) {
    this.getTranslations();
    const passedData = JSON.parse(data);
    const photoName = passedData.name;

    // post to API
    this.sendAPI.postResponse(image, hash, name, data, token)
      .then(response => {
        console.log('DAFM server response', JSON.stringify(response));
        if (response.status === 200) {
          /**
           * @todo fix mark as sent function by position or ID
           */

          this.photoService.markAsSent(photoName);
          const t = this.toast.create({
            message: this.success,
            position: 'top',
            duration: 3000,
            cssClass: 'custom-toast',
            buttons: [
              {
                side: 'start',
                icon: 'send',
                handler: () => {
                  console.log('');
                }
              }
            ]
          })
          // tslint:disable-next-line: no-shadowed-variable
          t.then(t => t.present());
        }
        this.photoService.deselectPhotos();

      })
      .catch(err => {
        if (err.status === 401) {
          const t = this.toast.create({
            message: this.expired,
            position: 'top',
            duration: 3000,
            cssClass: 'custom-toast-warning',
            buttons: [
              {
                side: 'start',
                icon: 'alert-circle-outline',
                handler: () => {
                  console.log('');
                },
              },
            ],
          });
          // tslint:disable-next-line: no-shadowed-variable
          t.then((t) => t.present());
          this.photoService.deselectPhotos();
          this.route.navigate(['login'])
        } else {
          console.log('DAFM error repsonse', JSON.stringify(err));
          const t = this.toast.create({
            message: this.failed,
            position: 'top',
            duration: 3000,
            cssClass: 'custom-toast-warning',
            buttons: [
              {
                side: 'start',
                icon: 'alert-circle-outline',
                handler: () => {
                  console.log('');
                }
              }
            ]
          })
          // tslint:disable-next-line: no-shadowed-variable
          t.then(t => t.present());
          this.photoService.deselectPhotos();

        }
      });

  }

  async getSingleFile(filePath: string): Promise<File> {
    // Get FileEntry from image path
    const fileEntry: FileEntry = await this.file.resolveLocalFilesystemUrl(filePath) as FileEntry;

    // Get File from FileEntry. Again note that this file does not contain the actual file data yet.
    const cordovaFile: any = await this.convertFileEntryToCordovaFile(fileEntry);

    // Use FileReader on the File object to populate it with the true file contents.
    return this.convertCordovaFileToJavascriptFile(cordovaFile);
  }
  /**
   * convert to a cordova file
   * @param "fileEntry"
   */
  private convertFileEntryToCordovaFile(fileEntry: FileEntry): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      fileEntry.file(resolve, reject);
    })
  }
  /**
   *  finally, convert cordiva file to JS file
   * @param "cordovaFile"
   */
  private convertCordovaFileToJavascriptFile(cordovaFile: any): Promise<File> {
    return new Promise<File>((resolve, reject) => {
      const reader = new FileReader();
      reader.onloadend = () => {
        if (reader.error) {
          reject(reader.error);
        } else {
          const blob: any = new Blob([reader.result], { type: cordovaFile.type });
          blob.lastModifiedDate = new Date();
          blob.name = cordovaFile.name;
          resolve(blob as File);
        }
      };
      reader.readAsArrayBuffer(cordovaFile);
    });
  }

  private getTranslations() {
    // tslint:disable-next-line: deprecation
    this.translate.get('Toast.Success').subscribe(
      value => {
        this.success = value;
      }
    )
    // tslint:disable-next-line: deprecation
    this.translate.get('Toast.Expired').subscribe(
      value => {
        this.expired = value;
      }
    )
    // tslint:disable-next-line: deprecation
    this.translate.get('Toast.Failed').subscribe(
      value => {
        this.failed = value;
      }
    )
  }
}