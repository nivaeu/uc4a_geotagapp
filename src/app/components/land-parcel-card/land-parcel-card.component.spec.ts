import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LandParcelCardComponent } from './land-parcel-card.component';

describe('LandParcelCardComponent', () => {
  let component: LandParcelCardComponent;
  let fixture: ComponentFixture<LandParcelCardComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ LandParcelCardComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LandParcelCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
