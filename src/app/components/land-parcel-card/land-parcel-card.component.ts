import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-land-parcel-card',
  templateUrl: './land-parcel-card.component.html',
  styleUrls: ['./land-parcel-card.component.scss'],
})
export class LandParcelCardComponent implements OnInit {
  @Input() title: string;
  @Input() address: string;
  @Input() description: string;
  @Input() image: string;
  @Input() status: string;

  constructor() { }

  ngOnInit() { }

}
