import { NgModule } from '@angular/core';
import { AuthGuard } from './guards/auth.guard';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    canActivate: [AuthGuard],
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/public/sso-login/sso-login.module').then(m => m.SsoLoginPageModule)
  },
  {
    path: 'settings',
    canActivate: [AuthGuard],
    loadChildren: () => import('./pages/settings/settings.module').then(m => m.SettingsPageModule)
  },
  {
    path: 'parcel-map/:id',
    canActivate: [AuthGuard],
    loadChildren: () => import('./pages/parcel-map/parcel-map.module').then(m => m.ParcelMapPageModule)
  },

  {
    path: 'terms-and-conditions',
    loadChildren: () => import('./pages/public/terms-and-conditions/terms-and-conditions.module').then(m => m.TermsAndConditionsPageModule)
  },
  {
    path: 'ar-camera/:id',
    loadChildren: () => import('./pages/ar-camera/ar-camera.module').then(m => m.ArCameraPageModule)
  },
  {
    path: 'single-scheme/:id',
    canActivate: [AuthGuard],
    loadChildren: () => import('./pages/single-scheme/single-scheme.module').then(m => m.SingleSchemePageModule)
  },
  {
    path: 'schemes',
    canActivate: [AuthGuard],
    loadChildren: () => import('./pages/schemes/schemes.module').then( m => m.SchemesPageModule)
  },



];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules, relativeLinkResolution: 'legacy' })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
