# UC4a GeoTag Photo App

## Introduction

This sub-project is part of the ["New IACS Vision in Action” --- NIVA](https://www.niva4cap.eu/) project that delivers a suite of digital solutions, e-tools and good practices for e-governance and initiates an innovation ecosystem to support further development of IACS that will facilitate data and information flows. This project has received funding from the European Union’s Horizon 2020 research and innovation programme under grant agreement No 842009. Please visit the [website](https://www.niva4cap.eu) for further information. A complete list of the sub-projects made available under the NIVA project can be found on [gitlab](https://gitlab.com/nivaeu/)

## Getting started

```bash
$ git clone git@github.com:niva......
$ cd niva
$ npm install
$ npm start
```

## Production build

```bash
$ npm run build

```

## iOS

We can run it as a ios application using XCode.


```bash
$ npx cap add ios
$ yarn run build 
$ npx cap sync
$ npx cap open ios
```

## Android

We can run it as a ios application using Android Studio.


```bash
$ npx cap add android
$ yarn run build 
$ npx cap sync
$ npx cap open android
```

## Full Documentation

Full documentation is available through the wiki: [WIKI](../../wikis)

## License

This project uses the following license: **EUPL**



